package com.amg.beercatalog.di

import android.content.Context
import androidx.annotation.NonNull
import com.amg.beercatalog.constants.BeerConstants
import com.amg.beercatalog.data.local.BeerDao
import com.amg.beercatalog.data.local.BeerDatabase
import com.amg.beercatalog.data.remote.BeerRemoteDataSource
import com.amg.beercatalog.data.remote.BeerService
import com.amg.beercatalog.data.repository.BeerRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson, @NonNull okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BeerConstants.API_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Singleton
    @Provides
    fun provideHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .writeTimeout(BeerConstants.API_TIMEOUT, TimeUnit.SECONDS)
        .connectTimeout(BeerConstants.API_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(BeerConstants.API_TIMEOUT, TimeUnit.SECONDS)
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .build()

    @Provides
    fun provideBeerService(retrofit: Retrofit): BeerService =
        retrofit.create(BeerService::class.java)

    @Provides
    @Singleton
    fun provideBeerRemoteDataSource(beerService: BeerService) = BeerRemoteDataSource(beerService)

    @Provides
    @Singleton
    fun provideBeerDatabase(@ApplicationContext appContext: Context) = BeerDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideBeerDao(db: BeerDatabase) = db.beerDao()

    @Singleton
    @Provides
    fun provideRepository(
        remoteDataSource: BeerRemoteDataSource,
        localDataSource: BeerDao) = BeerRepository(remoteDataSource, localDataSource)


}