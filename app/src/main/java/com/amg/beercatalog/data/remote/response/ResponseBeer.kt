package com.amg.beercatalog.data.remote.response

import com.amg.beercatalog.data.entity.Beer

data class ResponseBeer(val beers: List<Beer>)