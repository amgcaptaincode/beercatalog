package com.amg.beercatalog.data.repository

import com.amg.beercatalog.data.local.BeerDao
import com.amg.beercatalog.data.remote.BeerRemoteDataSource
import com.amg.beercatalog.utils.performGetOperation
import javax.inject.Inject

class BeerRepository @Inject constructor(
    private val remoteDataSource: BeerRemoteDataSource,
    private val localDataSource: BeerDao
) {

    fun getBeerCatalog(page: Int, perPage: Int) = performGetOperation(
        databaseQuery = { localDataSource.getBeerByPage(page, perPage) },
        networkCall = { remoteDataSource.getBeerCatalog(page, perPage) },
        saveCallResult = {
            for (item in it) {
                item.page = page
            }
            localDataSource.insertAll(it)
        }
    )

    suspend fun deleteAll() {
        localDataSource.deleteAll()
    }

}