package com.amg.beercatalog.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.amg.beercatalog.data.entity.Beer

@Dao
interface BeerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(beerCatalog: List<Beer>)

    @Query("SELECT * FROM Beer WHERE page = :page limit :perPage")
    fun getBeerByPage(page: Int, perPage: Int): LiveData<List<Beer>>

    @Query("DELETE FROM Beer")
    suspend fun deleteAll()

}