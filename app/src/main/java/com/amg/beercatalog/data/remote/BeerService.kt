package com.amg.beercatalog.data.remote

import com.amg.beercatalog.data.entity.Beer
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface BeerService {

    @GET("beers")
    suspend fun getBeerCatalog(
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): Response<List<Beer>>

}