package com.amg.beercatalog.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.amg.beercatalog.constants.BeerConstants
import com.amg.beercatalog.data.entity.Beer
import com.amg.beercatalog.utils.StringListConverter

@Database(
    entities = [Beer::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(
    value = [
        (StringListConverter::class)
    ]
)
abstract class BeerDatabase : RoomDatabase() {

    abstract fun beerDao(): BeerDao

    companion object {

        @Volatile
        private var instance: BeerDatabase? = null

        fun getDatabase(context: Context): BeerDatabase =
            instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context, BeerDatabase::class.java, BeerConstants.BEER_DATABASE)
                .fallbackToDestructiveMigration()
                .build()

    }

}