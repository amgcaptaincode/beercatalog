package com.amg.beercatalog.data.remote

import com.amg.beercatalog.utils.Resource
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException

abstract class BaseDataSource {

    protected suspend fun <T> getResponse(call: suspend () -> Response<T>): Resource<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return Resource.success(body)
            }
            return error(" ${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return if (e is IOException) {
                error("Check the internet connection")
            } else if (e is HttpException || e is SocketTimeoutException) {
                error("Information cannot be retrieved. Please try later.")
            } else {
                error("Information cannot be retrieved. Please try later.")
            }
        }
    }

    private fun <T> error(message: String): Resource<T> {
        return Resource.error("Network call has failed for a following reason: $message")
    }

}