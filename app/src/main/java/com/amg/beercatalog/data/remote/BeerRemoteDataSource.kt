package com.amg.beercatalog.data.remote

import javax.inject.Inject

class BeerRemoteDataSource @Inject constructor(
    private val beerService: BeerService
) : BaseDataSource() {

    suspend fun getBeerCatalog(page: Int, perPage: Int) =
        getResponse { beerService.getBeerCatalog(page, perPage) }

}