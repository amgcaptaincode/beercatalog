package com.amg.beercatalog.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.amg.beercatalog.R
import com.amg.beercatalog.constants.BeerConstants
import com.amg.beercatalog.data.entity.Beer
import com.amg.beercatalog.databinding.FragmentMasterBinding
import com.amg.beercatalog.ui.adapters.BeerAdapter
import com.amg.beercatalog.utils.Resource
import com.amg.beercatalog.viewmodel.BeerViewModel
import com.softrunapps.paginatedrecyclerview.PaginatedAdapter

class MasterFragment : Fragment(), BeerAdapter.BeerListener {

    private var _binding: FragmentMasterBinding? = null

    private val binding get() = _binding!!

    private val viewModel: BeerViewModel by activityViewModels()

    lateinit var navigationListener: NavigationListener

    private lateinit var adapter: BeerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigationListener = try {
            requireContext() as NavigationListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implemented NavigationListener")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null) {
            _binding = FragmentMasterBinding.inflate(inflater, container, false)

            setupRecyclerView()
            initListeners()
            setupObservers()
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getBeerCatalog(1)
    }

    private fun setupRecyclerView() {

        binding.rvBeer.layoutManager = GridLayoutManager(requireContext(), 2)

        adapter = BeerAdapter(this)
        adapter.startPage = 1
        adapter.setPageSize(BeerConstants.PER_PAGE)
        adapter.recyclerView = binding.rvBeer

        adapter.setOnPaginationListener(object : PaginatedAdapter.OnPaginationListener {
            override fun onFinish() {
            }

            override fun onCurrentPage(page: Int) {
            }

            override fun onNextPage(page: Int) {
                viewModel.getBeerCatalog(page)
            }

        })

    }

    private fun initListeners() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.setRefreshing(true)
            adapter.clear()
            viewModel.deleteAll()
            viewModel.getBeerCatalog(1)
        }
    }

    private fun setupObservers() {

        viewModel.beerCatalogListLiveData.observe(viewLifecycleOwner, Observer {
            if (binding.swipeRefreshLayout.isRefreshing)
                viewModel.setRefreshing(false)
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    val beerCatalogList = it.data
                    if (!beerCatalogList.isNullOrEmpty())
                        adapter.submitItems(ArrayList(beerCatalogList))
                }
                Resource.Status.ERROR ->
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING ->
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.label_loading_beer_catalog),
                        Toast.LENGTH_SHORT
                    ).show()
            }
        })

        viewModel.isRefreshing.observe(viewLifecycleOwner, Observer {
            it.let {
                binding.swipeRefreshLayout.isRefreshing = it
            }
        })

    }


    interface NavigationListener {

        fun gotoDetail(beer: Beer)

    }

    override fun onClickedItem(view: View, beer: Beer) {
        navigationListener.gotoDetail(beer)
    }

}