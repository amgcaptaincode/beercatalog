package com.amg.beercatalog.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amg.beercatalog.databinding.ItemFoodPairingBinding

class FoodPairingAdapter() : RecyclerView.Adapter<FoodPairingHolder>() {

    private val items = ArrayList<String>()

    fun setItems(mItems: ArrayList<String>) {
        this.items.clear()
        this.items.addAll(mItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodPairingHolder {
        val binding: ItemFoodPairingBinding =
            ItemFoodPairingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FoodPairingHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: FoodPairingHolder, position: Int) {
        holder.bind(items[position])
    }

}

class FoodPairingHolder(
    private val itemFoodPairingBinding: ItemFoodPairingBinding
) : RecyclerView.ViewHolder(itemFoodPairingBinding.root) {

    fun bind(item: String) {

        itemFoodPairingBinding.tvFoodPairing.text = item

    }

}
