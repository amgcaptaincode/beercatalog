package com.amg.beercatalog.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amg.beercatalog.data.entity.Beer
import com.amg.beercatalog.databinding.ItemBeerBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.softrunapps.paginatedrecyclerview.PaginatedAdapter

class BeerAdapter(private val listener: BeerListener) :
    PaginatedAdapter<Beer, BeerViewHolder>() {

    interface BeerListener {
        fun onClickedItem(view: View, beer: Beer)
    }

    private val items = ArrayList<Beer>()

    fun setItems(mItems: ArrayList<Beer>) {
        this.items.clear()
        this.items.addAll(mItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder {
        val binding: ItemBeerBinding =
            ItemBeerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BeerViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) =
        holder.bind(getItem(position))

}

class BeerViewHolder(
    private val itemBinding: ItemBeerBinding,
    private val listener: BeerAdapter.BeerListener
) : RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(item: Beer) {

        itemBinding.tvName.text = item.name
        itemBinding.tvTagline.text = item.tagLine

        item.imageUrl.let {
            Glide.with(itemBinding.root)
                .load(it)
                .transform(CenterInside())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(itemBinding.ivBeer)
        }

        itemBinding.root.setOnClickListener {
            listener.onClickedItem(itemBinding.ivBeer, item)
        }

    }

}