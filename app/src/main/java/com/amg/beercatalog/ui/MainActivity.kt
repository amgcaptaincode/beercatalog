package com.amg.beercatalog.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.findNavController
import com.amg.beercatalog.R
import com.amg.beercatalog.data.entity.Beer
import com.amg.beercatalog.ui.MasterFragmentDirections.*
import com.amg.beercatalog.viewmodel.BeerViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(),
    MasterFragment.NavigationListener {

    private val viewModel by viewModels<BeerViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun gotoDetail(beer: Beer) {
        findNavController(R.id.beer_catalog_navigation_container).navigate(
            actionMasterFragmentToDetailFragment(beer)
        )
    }
}