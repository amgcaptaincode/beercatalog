package com.amg.beercatalog.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.amg.beercatalog.R
import com.amg.beercatalog.data.entity.Beer
import com.amg.beercatalog.databinding.FragmentDetailBinding
import com.amg.beercatalog.ui.adapters.FoodPairingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!

    val args: DetailFragmentArgs by navArgs()

    private lateinit var adapter: FoodPairingAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)

        setupToolbar()
        setupRecyclerView()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val beer = args.beer
        setBeerDetail(beer)

    }

    private fun setupToolbar() {
        binding.beerDetailToolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        binding.beerDetailToolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun setupRecyclerView() {
        adapter = FoodPairingAdapter()
        binding.contentDetail.rvFoodPairing.adapter = adapter
    }

    @SuppressLint("SetTextI18n")
    private fun setBeerDetail(beer: Beer) {

        adapter.setItems(beer.foodPairing as ArrayList<String>)
        binding.beerDetailToolbar.title = beer.name

        binding.contentDetail.tvDetailTagline.text = "${beer.tagLine} (${beer.firstBrewed})"
        binding.contentDetail.tvDetailDescription.text = beer.description

        beer.imageUrl.let {
            Glide.with(binding.root)
                .load(it)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(binding.ivDetailBeer)
        }
    }

}