package com.amg.beercatalog.constants

object BeerConstants {

    const val API_URL = "https://api.punkapi.com/v2/"

    const val API_TIMEOUT = 45L

    const val BEER_DATABASE = "beer_db"

    const val PER_PAGE = 20

}