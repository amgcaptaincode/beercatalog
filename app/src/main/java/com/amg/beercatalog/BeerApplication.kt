package com.amg.beercatalog

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BeerApplication : Application()