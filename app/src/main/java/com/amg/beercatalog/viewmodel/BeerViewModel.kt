package com.amg.beercatalog.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.amg.beercatalog.constants.BeerConstants
import com.amg.beercatalog.data.entity.Beer
import com.amg.beercatalog.data.repository.BeerRepository
import com.amg.beercatalog.utils.AbsentLiveData
import com.amg.beercatalog.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BeerViewModel @ViewModelInject constructor(
    private val repository: BeerRepository
) : ViewModel() {

    private var beerCatalogLiveData: MutableLiveData<Int?> = MutableLiveData()
    val beerCatalogListLiveData: LiveData<Resource<List<Beer>>>

    private val _isRefreshing = MutableLiveData<Boolean>()
    val isRefreshing: LiveData<Boolean>
        get() = _isRefreshing

    init {
        beerCatalogListLiveData = beerCatalogLiveData.switchMap {
            beerCatalogLiveData.value?.let {
                repository.getBeerCatalog(
                    it,
                    BeerConstants.PER_PAGE
                )
            }
                ?: AbsentLiveData.create()
        }
    }

    fun getBeerCatalog(page: Int) = beerCatalogLiveData.postValue(page)

    fun deleteAll() = viewModelScope.launch {
        repository.deleteAll()
    }

    fun setRefreshing(isRefresh: Boolean) {
        _isRefreshing.value = isRefresh
    }

}